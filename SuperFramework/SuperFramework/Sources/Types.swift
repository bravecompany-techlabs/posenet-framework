import Foundation

public struct Vector2D : Codable{
    var x:Float, y:Float
}

public struct Vector2DInt  : Codable{
    var x:Int, y:Int
}

public struct PartWithScore  : Codable{
    var score: Float = 0
    var part: Part
}

public struct Part  : Codable{
    var heatmapX: Int
    var heatmapY: Int
    var id: Int
}

public struct Keypoint  : Codable{
    var score: Float
    var position: Vector2D
    var part: String
}

public struct Pose  : Codable{
    var keypoints: [Keypoint]
    var score: Float
}

public func half(_ k: Int) -> Int {
    return Int(floor(Double(k/2)))
}




import Foundation
import UIKit
import Vision
import TensorSwift
import AVFoundation

//import CoreMLHelper

open class PosenetManager : NSObject {
    
   @objc static public let shared = PosenetManager();
    
    private let mlObjectDetectionQueue = DispatchQueue(label: "com.bravecompany.super.dispatchqueue")
    
    
    let posenet = PoseNet()
    var isXcode : Bool = false // true: localfile , false: device camera
    
    // controlling the pace of the machine vision analysis
    var lastAnalysis: TimeInterval = 0
    var pace: TimeInterval = 0.08 // in seconds, classification will not repeat faster than this value
    // performance tracking
    let trackPerformance = false // use "true" for performance logging
    var frameCount = 0
    let framesPerSample = 10
    var startDate = NSDate.timeIntervalSinceReferenceDate
    let semaphore = DispatchSemaphore(value: 1)

    
    //let model = posenet513_v1_075()
    let model = posenet513()
    //let model = posenet257()
    
    let targetImageSize = CGSize(width: 513, height: 513)
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    let videoQueue = DispatchQueue(label: "videoQueue")
    let drawQueue = DispatchQueue(label: "drawQueue")
    var captureSession = AVCaptureSession()
    var captureDevice: AVCaptureDevice?
    let videoOutput = AVCaptureVideoDataOutput()
    var isWriting : Bool = false
    
    public override init()
    {
        NSLog("PosenetManager Hello!")
    }
    
    
    @objc open func analysisImageTest() -> String?
    {
        let fname = "tennis_in_crowd.jpg"
        
        if let image = UIImage(named: fname)?.resize(to: targetImageSize)
        {
           // previewView.image = image
            let result = measure(
                runCoreML(
                    image.pixelBuffer(width: Int(targetImageSize.width),
                                      height: Int(targetImageSize.height))!
                )
            )
            
            print(result.duration)
            
            print("Result Count:", result.result.count)
            
            let json : String! = toPoseJson(result.result[0])
            print(json)
            
            return json
        }
        else
        {
            NSLog("image file not founnd");
            return nil
        }
    }
    
    @objc public func analysisImage(_ targetImage: UIImage?) -> String?
    {
        // let fname = "tennis_in_crowd.jpg"
        // let image = targetImage.resize(to: targetImageSize)
        
        if let image = targetImage?.resize(to: targetImageSize)
        {
            // previewView.image = image
            let result = measure(
                runCoreML(
                    image.pixelBuffer(width: Int(targetImageSize.width),
                                      height: Int(targetImageSize.height))!
                )
            )
            
            print(result.duration)
            print("Person Count:", result.result.count)
            
            let json : String? = toPoseListJson(result.result)
            print("Pose Result:", json)
            return json;
            
           // return json as! NSString
            
//            if result.result.count > 0
//            {
//                let json : String? = toPoseJson(result.result[0])
//                print("Pose Result:", json)
//                return json
//            }
//            else
//            {
//                print ("Pose Not Found!")
//            }
            
        }
        else
        {
            NSLog("image file not founnd");
            return nil;
        }
    }
    
//
//    @objc public func analysisByteImage(_ bytes: [UInt8], width: Int, height: Int, scale: CGFloat = 0, orientation: UIImage.Orientation = .up) -> String?
//    {
//
//        // convert byte array to UIImage
//        guard let image = UIImage.fromByteArrayRGBA(bytes, width: width, height: height, scale: scale, orientation: UIImage.Orientation(orientation))
//            else {
//                print("cound not continue - no create a UIImage from RGBA byte array")
//                return nil
//            }
//
//        return analysisImage(image);
//    }

    @objc public func analysisByteImage(_ bytes: [UInt8], width: Int, height: Int, scale: CGFloat = 0, orientation: UIImage.Orientation = .up) -> String?
    {
        let bytelength = bytes.count;
        
        // convert byte array to UIImage
        guard let image = UIImage.fromByteArrayRGBA(bytes, width: width, height: height, scale: scale, orientation: UIImage.Orientation(orientation))
            else {
                print("cound not continue - no create a UIImage from RGBA byte array")
                return nil
        }
        
        return analysisImage(image);
    }
    
    
    @objc public func analysisPixelBuffer(_ img: CVPixelBuffer?, srcWidth1: Int , srcHeight1: Int) -> String?
    {
        let result = analysisPixelBufferToPose(img!, srcWidth: srcWidth1, srcHeight: srcHeight1)
        
        let json : String? = toPoseJson(result[0])
        
        print("Pose Result:", json)
        
        return json
        
    }
        
    
    public func analysisPixelBufferToPose(_ img: CVPixelBuffer, srcWidth: Int , srcHeight: Int) -> [Pose]
    {
        //let helper : PixelBufferHelper =  PixelBufferHelper()
        
        if Int(targetImageSize.width) != srcWidth || Int(targetImageSize.height) != srcHeight
        {
            let pixWidth = CVPixelBufferGetWidth(img)
            
            NSLog("Pixel Width : %d", pixWidth)
            
            //let newimg = resizePixelBuffer(img, width: Int(targetImageSize.width),height: Int(targetImageSize.height))!
            
            let newimg = resizePixelBuffer(img, cropX: 0, cropY: 0, cropWidth: srcWidth, cropHeight: srcHeight, scaleWidth:Int(targetImageSize.width), scaleHeight: Int(targetImageSize.height) )!
            
            let pixWidth2 = CVPixelBufferGetWidth(newimg)
            let pixHeight2 = CVPixelBufferGetHeight(newimg)
            let fomatinfo = CVPixelBufferGetPixelFormatType(newimg);
            
             NSLog("New Pixel Width : %d, %d", pixWidth2,pixHeight2 )
            
            return runCoreML(newimg)
            
            //return runCoreML(img)
        }
        else
        {
            return runCoreML(img)
        }
    }
    
    public func toPoseJson(_ pose : Pose) -> String?
    {
        let encoder = JSONEncoder();
        do
        {
            let newJson = try? encoder.encode(pose)
            
            if let jsonData = newJson, let jsonString = String(data: jsonData, encoding: .utf8)
            {
               // print(jsonString)
                
                return jsonString
            }
            
            return nil;
        }
        catch
        {
            print(error)
            return nil
        }
    }
    
    
    public func toPoseListJson(_ poses : [Pose]) -> String?
    {
        let encoder = JSONEncoder();
        
        do
        {
            let newJson = try? encoder.encode(poses)
            
            if let jsonData = newJson, let jsonString = String(data: jsonData, encoding: .utf8)
            {
                return jsonString
            }
            
            return nil;
        }
        catch
        {
            print(error)
            return nil
        }
    }
    
    
//    func analysisVideo()
//    {
//        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
//        previewView.layer.addSublayer(previewLayer)
//    }
    
    public func runCoreML(_ img: CVPixelBuffer) -> [Pose]
    {
    
        let result = try? model.prediction(image__0: img)
            
        let tensors = result?.featureNames.reduce(into: [String: Tensor]()) {
                $0[$1] = getTensor(
                    result?.featureValue(for: $1)?.multiArrayValue)
        }
        
        let sum = tensors!["heatmap__0"]!.reduce(0, +) / (17 * 33 * 33)
            print(sum)
            
        let poses = posenet.decodeMultiplePoses(
                scores: tensors!["heatmap__0"]!,
                offsets: tensors!["offset_2__0"]!,
                displacementsFwd: tensors!["displacement_fwd_2__0"]!,
                displacementsBwd: tensors!["displacement_bwd_2__0"]!,
                outputStride: 16, maxPoseDetections: 15,
                scoreThreshold: 0.5,nmsRadius: 20)
            
        return poses
    }
    
    
    /*********************************************************************
     *
     *
     *********************************************************************/
        

    let context = CIContext()
    var rotateTransform: CGAffineTransform?
    var scaleTransform: CGAffineTransform?
    var cropTransform: CGAffineTransform?
    var resultBuffer: CVPixelBuffer?

    func croppedSampleBuffer(_ sampleBuffer: CMSampleBuffer, targetSize: CGSize) -> CVPixelBuffer? {
        
        guard let imageBuffer: CVImageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            fatalError("Can't convert to CVImageBuffer.")
        }
        
        // Only doing these calculations once for efficiency.
        // If the incoming images could change orientation or size during a session, this would need to be reset when that happens.
        if rotateTransform == nil {
            let imageSize = CVImageBufferGetEncodedSize(imageBuffer)
            let rotatedSize = CGSize(width: imageSize.height, height: imageSize.width)
            
            guard targetSize.width < rotatedSize.width, targetSize.height < rotatedSize.height else {
                fatalError("Captured image is smaller than image size for model.")
            }
            
            let shorterSize = (rotatedSize.width < rotatedSize.height) ? rotatedSize.width : rotatedSize.height
            rotateTransform = CGAffineTransform(translationX: imageSize.width / 2.0, y: imageSize.height / 2.0).rotated(by: -CGFloat.pi / 2.0).translatedBy(x: -imageSize.height / 2.0, y: -imageSize.width / 2.0)
            
            let scale = targetSize.width / shorterSize
            scaleTransform = CGAffineTransform(scaleX: scale, y: scale)
            
            // Crop input image to output size
            let xDiff = rotatedSize.width * scale - targetSize.width
            let yDiff = rotatedSize.height * scale - targetSize.height
            cropTransform = CGAffineTransform(translationX: xDiff/2.0, y: yDiff/2.0)
        }
        
        // Convert to CIImage because it is easier to manipulate
        let ciImage = CIImage(cvImageBuffer: imageBuffer)
        let rotated = ciImage.transformed(by: rotateTransform!)
        let scaled = rotated.transformed(by: scaleTransform!)
        let cropped = scaled.transformed(by: cropTransform!)
        
        // Note that the above pipeline could be easily appended with other image manipulations.
        // For example, to change the image contrast. It would be most efficient to handle all of
        // the image manipulation in a single Core Image pipeline because it can be hardware optimized.
        
        // Only need to create this buffer one time and then we can reuse it for every frame
        if resultBuffer == nil {
            let result = CVPixelBufferCreate(kCFAllocatorDefault, Int(targetSize.width), Int(targetSize.height), kCVPixelFormatType_32BGRA, nil, &resultBuffer)
            
            guard result == kCVReturnSuccess else {
                fatalError("Can't allocate pixel buffer.")
            }
        }
        
        // Render the Core Image pipeline to the buffer
        context.render(cropped, to: resultBuffer!)
        
        //  For debugging
        //  let image = imageBufferToUIImage(resultBuffer!)
        //  print(image.size) // set breakpoint to see image being provided to CoreML
        
        return resultBuffer
    }

}
